import store from '../store';

const DataResource = {
  start(config) {
    const table = $(config.id)
      .on('preXhr.dt', (e, settings, data) => {
        if (data.order.length > 0) {
          let orderingColumn = data.columns[data.order[0].column];
          if (data.order[0].dir === 'asc') {
            data.sort = orderingColumn.data;
          } else {
            data.sort = `-${orderingColumn.data}`;
          }
        }

        if (data.sort === 'listing_id') {
          delete data.sort;
        }

        data.filter = {};
        // <----- DATA FILTERING USED IN SEARCH ----->
        if (data.search.value) {
          data.columns.forEach(column => {
            if (column.searchable) {
              data.filter[column.data] = data.search.value;
            }
          });

          if (config.additionalSearch.length) {
            config.additionalSearch.forEach(column => {
              data.filter[column] = data.search.value;
            });
          }
        }

        // <----- DATA PAGINATION ----->
        if (data.start === 0) {
          data.page = 1;
        } else {
          data.page = data.start / data.length + 1;
        }
        data.per_page = data.length;

        delete data.columns;
      })
      .on('xhr.dt', (e, settings, json, xhr) => {
        json.recordsTotal = json.meta.total;
        json.recordsFiltered = json.meta.total;
      })
      .DataTable({
        asStripeClasses: [],
        ajax: {
          url: `${process.env.MIX_API_BASE_URL}/${config.url}`,
          dataSrc: 'data',
          async: true,
          beforeSend: function(request) {
            if (store.getters.isLoggedIn) {
              request.setRequestHeader('Authorization', `Bearer ${store.getters.token}`);
            }
          }
        },
        bAutoWidth: false,
        responsive: true,
        processing: true,
        serverSide: true,
        lengthMenu: [5, 10, 15, 20],
        pageLength: 10,
        pagingType: 'numbers',
        language: {
          processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>',
          emptyTable: 'No data available.'
        },
        columns: config.columns,
        columnDefs: config.definitions,
        searching: config.search,
        ordering: config.order
      });
    return table;
  }
};
export default {
  initialize(config) {
    $(document).ready(() => {
      const table = DataResource.start(config);
    });
  },
  refresh(id) {
    $(id)
      .DataTable()
      .ajax.reload();
  },
  destroy(id) {
    $(id)
      .DataTable()
      .destroy();
  }
};
