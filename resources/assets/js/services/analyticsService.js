import $axios from './axiosService';


export default {
  fetch() {
    return $axios().get('/api/v1/analytics');
  },
  fetchUsers(start, end) {
    return $axios().get(`/api/v1/analytics/users?start_date=${start}&end_date=${end}`);
  },
  fetchOrders(start, end) {
    return $axios().get(`/api/v1/analytics/orders?start_date=${start}&end_date=${end}`);
  },
  fetchTransactions(start, end) {
    return $axios().get(`/api/v1/analytics/transactions?start_date=${start}&end_date=${end}`);
  },
};
