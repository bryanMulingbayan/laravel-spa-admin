import $axios from './axiosService';

export default {
  login({ username, password }) {
    const params = {
      grant_type: 'password',
      client_id: process.env.MIX_API_CLIENT_ID,
      client_secret: process.env.MIX_API_CLIENT_SECRET,
      username,
      password,
      scope: '',
    };

    return $axios().post('/oauth/token', params);
  },

  getCurrentUser() {
    return $axios().get('/api/users/current');
  },
};
