import axios from 'axios';
import store from '../store';

export default () => {
  const $axios = axios.create({
    baseURL: process.env.MIX_API_BASE_URL,
  });

  $axios.defaults.headers.common['Content-Type'] = 'application/json';
  $axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

  if (store.getters.isLoggedIn) {
    $axios.defaults.headers.common.Authorization = `Bearer ${store.getters.token}`;
  }

  return $axios;
};
