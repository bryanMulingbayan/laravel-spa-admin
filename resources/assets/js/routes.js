import Vue from 'vue';
import Router from 'vue-router';
import store from './store';

import MainIndex from './views/MainIndex';
import Analytics from './views/Analytics';
import StoresIndex from './views/Stores/Index.vue';
import StoresList from './views/Stores/List.vue';
import AddStore from './views/Stores/Add.vue';
import Users from './views/Users.vue';
import Orders from './views/Orders.vue';
import Cards from './views/Cards.vue';
import Transactions from './views/Transactions.vue';
import Reviews from './views/Reviews.vue';

import Login from './views/Login';

Vue.use(Router);

export default {
    mode: 'history',
    linkActiveClass: 'active',
    linkExactActiveClass: 'exact-active',
    routes: [
        {
            path: '/login',
            component: Login,
            name: 'login',
            beforeEnter: (to, from, next) => {
                if (store.getters.isLoggedIn) {
                  next('/');
                } else {
                  next();
                }
            },
        },
        {
            path: '/',
            component: MainIndex,
            redirect: '/analytics',
            beforeEnter: (to, from, next) => {
                if (store.getters.isLoggedIn) {
                    next();
                } else {
                    next('/login');
                }
            },
            children: [
                {
                    path: 'analytics',
                    name: 'analytics',
                    component: Analytics,
                },
                {
                    path: 'stores',
                    component: StoresIndex,
                    children: [
                      {
                        path: '/',
                        name: 'stores-list',
                        component: StoresList,
                      },
                      {
                        path: 'add',
                        name: 'add-store',
                        component: AddStore,
                      },
                    ],
                },
                {
                    path: 'users',
                    name: 'users',
                    component: Users,
                },
                {
                    path: 'orders',
                    name: 'orders',
                    component: Orders,
                },
                {
                    path: 'cards',
                    name: 'cards',
                    component: Cards,
                },
                {
                    path: 'transactions',
                    name: 'transactions',
                    component: Transactions,
                },
                {
                    path: 'reviews',
                    name: 'reviews',
                    component: Reviews,
                }
            ]
        },
    ]
}