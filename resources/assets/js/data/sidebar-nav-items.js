export default function () {
  return [
    {
      title: 'Analytics',
      to: {
        name: 'analytics',
      },
      htmlBefore: '<i class="material-icons">insert_chart_outlined</i>',
      htmlAfter: '',
    },
    {
      title: 'Stores',
      htmlBefore: '<i class="material-icons">store</i>',
      to: {
        name: 'stores-list',
      },
    },
    {
      title: 'Users',
      htmlBefore: '<i class="material-icons">person_outline</i>',
      to: {
        name: 'users',
      },
    },
    {
      title: 'Cards',
      htmlBefore: '<i class="material-icons">credit_card</i>',
      to: {
        name: 'cards',
      },
    },
    {
      title: 'Orders',
      htmlBefore: '<i class="material-icons">fastfood</i>',
      to: {
        name: 'orders',
      },
    },
    {
      title: 'Transactions',
      htmlBefore: '<i class="material-icons">local_atm</i>',
      to: {
        name: 'transactions',
      },
    },
    {
      title: 'Reviews',
      htmlBefore: '<i class="material-icons">credit_card</i>',
      to: {
        name: 'reviews',
      },
    },
  ];
}
