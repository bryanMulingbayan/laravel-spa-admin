/* eslint no-param-reassign: "error" */

import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    auth: JSON.parse(localStorage.getItem('auth')) || {},
    user: JSON.parse(localStorage.getItem('user')) || {},
  },
  mutations: {
    setAuth(state, data) {
      state.auth = data;
      localStorage.setItem('auth', JSON.stringify(data));
    },
    clearAuth(state) {
      localStorage.removeItem('auth');
      localStorage.removeItem('user');
      state.auth = {};
      state.user = {};
    },
    setUser(state, user) {
      state.user = user || {};
      localStorage.setItem('user', JSON.stringify(user));
    },
  },
  getters: {
    token(state) {
      return state.auth.access_token;
    },
    isLoggedIn(state) {
      return 'access_token' in state.auth;
    }
  },
});
