require('./bootstrap');

import Vue from 'vue';
import ShardsVue from 'shards-vue'
import VueRouter from 'vue-router';
import routes from './routes';
import store from './store';
import Default from './layouts/Default.vue';

Vue.use(ShardsVue);
Vue.use(VueRouter);
Vue.component('default-layout', Default);

Vue.prototype.$eventHub = new Vue();

const app = new Vue({
    el: '#app',
    router: new VueRouter(routes),
    store
});
